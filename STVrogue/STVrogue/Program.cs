﻿// The pre-processing directive below should be the first non-comment line of your file.
// Keep it during development. When you release the software, remove it.

#define TEST_SETUP
using System;
using System.IO;
using System.Net.Mime;
using System.Reflection;
using STVrogue.GameLogic;
using STVrogue.Utils;

namespace STVrogue
{
    /// <summary>
    /// This contains the top-level main of STV Rogue, which in turn will call
    /// <see cref="GameRunner"/>, where the game main-loop is implemented.
    /// </summary>
    class Program
    {
        public static void Main(string[] args)
        {
            // (1) reading the configuration of the game-level to generate
            GameConfiguration conf = new GameConfiguration("rogueconfig.txt");
            Console.WriteLine("You want to play STVRogue. Should I record the play? (y/n)");
            var c = Console.ReadKey().KeyChar;
            if (c == 'y')
            { 
                // This is for Part-2 the Project. Ignore this during Part-1.
                //throw new NotImplementedException(); 
                Console.WriteLine("What config file do you want to use? (give the name of the text file in the ../STVRogue/saved/..)");
                conf = new GameConfiguration(Console.ReadLine());
            }
            // (2) create an instance of a Game:
            Game game = new Game(conf);
            // (3) attach an I/O Console to write texts to, and read from:
            // game.GameConsole = new InstrumentedGameConsole();
            if (c == 'y')
                game.GameConsole = new InstrumentedGameConsole();
            else
                game.GameConsole = new GameConsole();
            // (4) attach the Game to a runner. The runner contains the logic of the game's
            // main-loop.
            GameRunner runner = new GameRunner(game);
            // (5) Run the main-loop:
            runner.Run();

            if (c == 'y')
            {
                Console.WriteLine("What do you want to name the recording?");
                string p = Console.ReadLine();

                InstrumentedGameConsole cons = (InstrumentedGameConsole)game.GameConsole;
                
                using (StreamWriter sw = new StreamWriter(@"..\..\..\..\..\Recordings\"+p))
                {
                    sw.WriteLine("" + conf.rndSeed
                                    + ":" + conf.numberOfRooms
                                    + ":" + conf.maxRoomCapacity
                                    + ":" + conf.dungeonShape
                                    + ":" + conf.initialNumberOfMonsters
                                    + ":" + conf.initialNumberOfHealingPots
                                    + ":" + conf.initialNumberOfRagePots
                                    + ":" + conf.difficultyMode);
                    sw.WriteLine("MsgIn");
                    foreach (string s in cons.MsgIn)
                    {
                        sw.WriteLine(s);
                    }

                    sw.WriteLine("MsgOut");
                    foreach (string s in cons.MsgOut)
                    {
                        sw.WriteLine(s);
                    }
                }
            }
        }
    }
}