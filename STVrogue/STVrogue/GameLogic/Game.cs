﻿using System;
using System.Collections.Generic;
using System.Linq;
using STVrogue.Utils;

namespace STVrogue.GameLogic
{
    /// <summary>
    /// This class represents the whole game-state of STVRogue. It also contains
    /// the method <see cref="Update"/> that performs a single turn update to
    /// the game state.
    ///
    /// The game's main-loop is put separately in the class <see cref="GameRunner"/>.
    /// 
    /// <para></para>
    /// The main methods for this class are:
    ///
    /// <list type="bullet">
    ///
    /// <item> The constructor, for creating an instance of this Game, with a 
    /// dungeon according to a given configuration. </item>
    /// 
    /// <item> The method <see cref="Update"/> to do a single turn update. This is called
    /// from the mainloop in <see cref="GameRunner"/>. </item>
    /// 
    /// <item> The method <see cref="Flee"/> for you to program the logic of fleeing creatures.</item>
    ///
    /// </list>
    /// </summary>
    public class Game 
    {
        #region fields and pro
        
        public GameConfiguration Config { get; }
        
        public Player Player { get; }
        
        public Dungeon Dungeon { get; }

        public bool Gameover { get; set; } = false;
        
        /// <summary>
        /// To count the number of passed turns. 
        /// </summary>
        public int TurnNumber { get; private set; } = 0;
        public bool Usedpotion { get; private set; }

        
        /// <summary>
        /// A <see cref="GameConsole"/> provides a text-based Console. You can print strings on this console,
        /// or read strings from it. Using this to handle your text I/O.
        /// NOTE: Don't read and write directly to System's Console.
        /// </summary>
        public GameConsole GameConsole { get; set;  }

        /// <summary>
        /// A random generator you can use for making random decisions. The type
        /// is intentionally set to be an instance of <see cref="IRandomGenerator"/>
        /// to prevent you from directly using <see cref="Random"/>.
        /// When testing the game you need a setup where all your random generators
        /// behave deterministically, to avoid your testing to become flaky.
        /// The code below will use an instance of <see cref="RandomGenerator"/>,
        /// which is NOT deterministic. 
        /// Check out the other implementation of <see cref="IRandomGenerator"/>, namely
        /// <see cref="STVControlledRandom"/>, or else write your own implementation.
        /// </summary>
        //IRandomGenerator rnd = new RandomGenerator();
        IRandomGenerator rnd = new STVControlledRandom();
        
        #endregion
        
        public Game()
        {
            Player = new Player("0");
            GameConsole = new GameConsole();
        }

        /// <summary>
        /// Try to create an instance of Game satisfying the specified configuration.
        /// It should throw an exception if it does not manage to generate a dungeon
        /// satisfying the configuration.
        /// </summary>
        public Game(GameConfiguration conf) : this()
        {

            Config = conf;

            STVControlledRandom.SetSeed(conf.rndSeed);
            int k = 1000; //amount of attempts to generate a valid dungeon, adjust if needed
            bool generated = false;
            for(int i = 0; i < k; i++)
            {
                Dungeon = new Dungeon(conf.dungeonShape, conf.numberOfRooms, conf.maxRoomCapacity);
                if(Dungeon.SeedMonstersAndItems(conf.initialNumberOfMonsters, conf.initialNumberOfHealingPots, conf.initialNumberOfRagePots))
                {
                    Player = new Player("0");
                    Player.Hp = Player.HpMax;
                    Player.Location = Dungeon.StartRoom;
                    generated = true;
                    break;
                }
            }
            if (!generated) { throw new ArgumentException("Could not generate a dungeon with the current configuration settings"); }
            
        }


        /// <summary>
        /// Cause a creature to flee a combat. This will take the creature to a neighboring
        /// room. This should not breach the capacity of that room. Note that fleeing a
        /// combat is not always possible --see the Project Document.
        /// The method returns true if fleeing was successful, else false.
        /// </summary>
        public bool Flee(Creature c)
        {
            List<Room> rooms = new List<Room>(c.Location.Neighbors);

            if (c is Monster)
            {
                while (rooms.Count > 0)
                {
                    Room room = rooms[rnd.NextInt(rooms.Count)];
                    if (room.Creatures.Count >= room.Capacity)
                    {
                        rooms.Remove(room);
                    }
                    else
                    {
                        c.Move(room);
                        return true;
                    }
                }
            }
            else if (c is Player)
            {
                while (rooms.Count > 0)
                {
                    Room room = rooms[rnd.NextInt(rooms.Count)];
                    if (room.RoomType == RoomType.EXITroom)
                    {
                        rooms.Remove(room);
                        continue;
                    }

                    if (Config.difficultyMode == DifficultyMode.NEWBIEmode)
                    {
                        c.Move(room);
                        return true;
                    }
                    
                    else if (Config.difficultyMode == DifficultyMode.NORMALmode ||
                             Config.difficultyMode == DifficultyMode.ELITEmode)
                    {
                        // if used potion, cant flee
                        if (Player.turnUsedLastPotion == TurnNumber - 1)
                        {
                            rooms.Remove(room);
                            continue;
                        }
                        // else if normal mode, flee
                        else if (Config.difficultyMode == DifficultyMode.NORMALmode)
                        {
                            c.Move(room);
                            return true;
                        }
                    }

                    if (Config.difficultyMode == DifficultyMode.ELITEmode)
                    {
                        if ((c as Player).Enraged)
                        {
                            rooms.Remove(room);
                        }
                        else
                        {
                            c.Move(room);
                            return true;
                        }
                    }
                }
            }
            GameConsole.WriteLines("Cannot flee.");
            return false;
        }
        
        public bool Move(Creature c, Room r)
        { 
            if (c is Player)
            {
                if (Player.Location.NumberOfMonsters == 0)
                {
                    GameConsole.WriteLines("You successfully moved!");
                    c.Move(r);
                    Player.Location = r;
                    return true;
                }
                GameConsole.WriteLines("You can't move with monsters in the room! try something else");
                return false;
            }
            if (c.Location != Player.Location)
            {
                if (r.NumberOfMonsters < r.Capacity)
                { 
                    c.Move(r);
                    return true;
                }
                return false;
            }
            return false;
        }

        public bool Attack(Creature c, Creature f)
        {
            if (c is Player)
            {
                if (c.Location == f.Location && c.Alive && f.Alive)
                {
                    GameConsole.WriteLines("You successfully attack " + f.Name + "!");
                    c.Attack(f);
                    if (!f.Alive)
                    {
                        Dungeon.Creatures.Remove(f);
                        Player.Location.Creatures.Remove(f);
                    }
                    return true;
                }
                GameConsole.WriteLines("You failed attacking!");
            }
            else
            {
                if (f is Player)
                {
                    if (f.Location == c.Location && f.Alive && c.Alive)
                    {
                        GameConsole.WriteLines("You got attacked by " + c.Name + "!");
                        c.Attack(f);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Pickup()
        {
            if (Player.Location.Items.Count() > 0)
            {
                foreach (Item itms in Player.Location.Items)
                {
                    Player.Bag.Add(itms);
                    Dungeon.Items.Remove(itms);
                }

                Player.Location.Items.Clear();
                return true;
            }
            GameConsole.WriteLines("there are no items to pick up!");
            return false;
        }

        public bool UseItem(Item i)
        {
            Player.Use(TurnNumber, i);
            Player.Bag.Remove(i);
            return true;
        }

        public void MonsterIncrement()
        {
            foreach (Monster mons in Dungeon.Creatures)
            {
                switch (rnd.NextInt(4))
                {
                    case 0:
                        break;
                    case 1:
                        Move(mons, mons.Location.Neighbors[rnd.NextInt(mons.Location.Neighbors.Count)]);
                        break;
                    case 2:
                        Flee(mons);
                        break;
                    case 3:
                        Attack(mons,Player);
                        break;
                }
            }
        }

        /// <summary>
        /// Perform a single turn-update on the game. In every turn, each creature
        /// is allowed to do one action. The player does and specified in the argument
        /// of this method. A monster can either do nothing, move, attack, or flee.
        /// See the Project Document that defines when these are possible.
        /// The order in which creatures execute their actions is left for you to decide.
        /// </summary>
        public void Update(Command playerAction)
        {
            GameConsole.WriteLines("", "** "  + Player.Name + " " + playerAction);
            bool result = false;
            switch (playerAction.Name)
            {
                case CommandType.ATTACK :
                    GameConsole.WriteLines("      Clang! Wooosh. WHACK!");
                    result = Attack(Player, Player.Location.Creatures.Find(c => c.Id == playerAction.Args[0]));//Player.Location.Creatures[Player.Location.Creatures.Count]);
                    break;
                case CommandType.FLEE:
                    GameConsole.WriteLines("      We knew you are a coward.");
                    result = Flee(Player);
                    break;
                case CommandType.MOVE:
                    GameConsole.WriteLines("      You try to move to the next room");
                    result = Move(Player, Player.Location.Neighbors.First(x => x.Id == playerAction.Args[0]));
                    break;
                case CommandType.DoNOTHING:
                    GameConsole.WriteLines("      Lazy. Start working!");
                    result = true;
                    break;
                case CommandType.PICKUP:
                    GameConsole.WriteLines("      You try to pick up some Items!");
                    result = Pickup();
                    break;
                case CommandType.USE:
                    result = UseItem(Player.Bag.First(x => x.Id == playerAction.Args[0]));
                    break;
            }
            if (result)
            {
                TurnNumber++;
                MonsterIncrement();
                if (Player.Enragecounter > 0) Player.Enragecounter--;
                else Player.Enraged = false;
            }

            if (Player.Location == Dungeon.ExitRoom && Player.Alive) Gameover = true;
        }
        
        
        /*
        //---------HELPER FUNCTIONS---------
        public List<Room> ParseWcap(IEnumerable<Room> rms)
        {
            List<Room> retlist = new List<Room>();
            foreach (Room r in rms)
            {
                if(r.Capacity > r.NumberOfMonsters) retlist.Add(r);
            }
            return retlist;
        }

        public List<Room> ParseWOExit(IEnumerable<Room> rms)
        {
            List<Room> retlist = new List<Room>();
            foreach (Room r in rms)
            {
                if(r.RoomType != RoomType.EXITroom) retlist.Add(r);
            }

            return retlist;
        }
        public Room ParseRndN(IEnumerable<Room> rms)
        {
            List<Room> retlist = new List<Room>();
            foreach (Room r in rms)
            {
                if(r.RoomType != RoomType.EXITroom) retlist.Add(r);
            }
            return retlist[rnd.NextInt(retlist.Count)];
        }
        */
    }
}
