﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using STVrogue.Utils;

namespace STVrogue.GameLogic
{
    
    /// <summary>
    /// Representing a dungeon. A dungeon consists of rooms, connected to from a graph.
    /// It has one unique starting room and one unique exit room. All rooms should be
    /// reachable from the starting room.
    /// </summary>
    public class Dungeon
    {

        #region Fields and Properties
        
        /// <summary>
        /// All rooms in the dungeon, including the start and exit rooms.
        /// </summary>
        public List<Room> Rooms { get; } = new List<Room>();
        public Room StartRoom { get; set; }
        public Room ExitRoom { get; set; }

        public DungeonShapeType DungeonShape { get; set; }

        // Use this to force deterministic random numbers generation for testing purposes.
        // IRandomGenerator randomGenerator = new STVControlledRandom();
        //IRandomGenerator randomGenerator = new RandomGenerator();
        private IRandomGenerator randomGenerator = new STVControlledRandom();

        #endregion

        protected Dungeon() { }
        
        /// <summary>
        /// Create a dungeon with the indicated number of rooms and the indicated shape.
        /// A dungeon shape can be "linear" (list-shaped), "tree", or "random".
        /// <list type="bullet">
        /// <item>
        /// A dungeon should have a unique start-room and a unique exit-room. </item>
        /// <item>
        /// All rooms in the dungeon must be reachable from the start-room. </item>
        /// <item>
        /// Each room is set to have a random capacity between 1 and the given maximum-capacity.
        /// Start and exit-rooms should have capacity 0. </item>
        /// </list>
        /// </summary>
        public Dungeon(DungeonShapeType shape, int numberOfRooms, int maximumRoomCapacity) : base()
        {
            if (numberOfRooms < 3 || maximumRoomCapacity < 0)
                throw new ArgumentException();
            
            switch (shape)
            {
                case DungeonShapeType.LINEARshape:
                    this.DungeonShape = DungeonShapeType.LINEARshape;
                    MkLinearDungeon(numberOfRooms, maximumRoomCapacity);
                    break;
                case DungeonShapeType.TREEshape:
                    this.DungeonShape = DungeonShapeType.TREEshape;
                    MkTreeDungeon(numberOfRooms, maximumRoomCapacity);
                    break;
                case DungeonShapeType.RANDOMshape:
                    this.DungeonShape = DungeonShapeType.RANDOMshape;
                    MkRandomDungeon(numberOfRooms, maximumRoomCapacity);
                    break;
            }
        }
        
        private void MkLinearDungeon(int numberOfRooms, int maximumRoomCapacity)
        {
            //Create start room
            Room prev = new Room("S", RoomType.STARTroom, 0);
            Rooms.Add(prev);
            StartRoom = prev;
            
            Room r = null;
            
            //Create all ordinary rooms
            for (int k = 1 ; k<numberOfRooms - 1; k++)
            {
                int capacity = randomGenerator.NextInt(maximumRoomCapacity + 1); //+ 1 ; // kutu note
                if (k == numberOfRooms - 2)
                    capacity = maximumRoomCapacity;
                r = new Room("R" + k, RoomType.ORDINARYroom, capacity); // kutu note
                Rooms.Add(r);
                
                prev.Connect(r);
                prev = r;
            }

            //Create the exit room
            Room exit = new Room("E", RoomType.EXITroom, 0);
            ExitRoom = exit;
            Rooms.Add(exit);
            prev.Connect(exit);
        }

        private void MkTreeDungeon(int numberOfRooms, int maximumRoomCapacity)
        {
            StartRoom = new Room("R" + 0, RoomType.STARTroom, 0);
            Rooms.Add(StartRoom);
            MkTreeDungeonWorker(StartRoom, numberOfRooms - 1, maximumRoomCapacity);

            foreach (Room r in ExitRoom.Neighbors)
            {
                if (r.RoomType == RoomType.ORDINARYroom)
                    r.Capacity = maximumRoomCapacity;
            }
        }
        
        private int MkTreeDungeonWorker(Room parent, int numberOfRooms, int maximumRoomCapacity)
        {
            int parentId = int.Parse(parent.Id.Substring(1));
            int freshId = parentId + 1;
            while (numberOfRooms > 0)
            {
                int capacity = randomGenerator.NextInt(maximumRoomCapacity + 1); //+ 1 ; // kutu note
                Room r;
                if (numberOfRooms == 1 && ExitRoom == null)
                {
                    r = new Room("R" + freshId, RoomType.EXITroom, 0);
                    ExitRoom = r;
                }
                else    
                    r = new Room("R" + freshId, RoomType.ORDINARYroom, capacity);
                Rooms.Add(r);
                parent.Connect(r);
                //int N = Math.Max(1, numberOfRooms / 3);
                int N = numberOfRooms / 3;
                int lastUsedId = MkTreeDungeonWorker(r, N, maximumRoomCapacity);
                freshId = lastUsedId + 1;
                numberOfRooms -= N + 1 ;
            }
            return freshId; // kutu minor note
        }
        
        void MkRandomDungeon(int numberOfRooms, int maximumRoomCapacity)
        {
            MkTreeDungeon(numberOfRooms, maximumRoomCapacity);

            for (int i = 0; i < int.Max(1, numberOfRooms / 3);)
            {
                int rndm1 = randomGenerator.NextInt(numberOfRooms);
                int rndm2 = randomGenerator.NextInt(numberOfRooms);

                if (rndm1 != rndm2 && !Rooms[rndm1].Neighbors.Contains(Rooms[rndm2]))
                {
                    Rooms[rndm1].Connect(Rooms[rndm2]);
                    i++;
                }
            }
            
            foreach (Room r in ExitRoom.Neighbors)
            {
                if (r.RoomType == RoomType.ORDINARYroom)
                    r.Capacity = maximumRoomCapacity;
            }
        }

        /// <summary>
        /// Returns the total capacity of the Dungeon. Used in the SMaI method for requirement testing.
        /// </summary>
        public int TotalDungeonCapacity()
        {
            int capacitySum = 0;
            foreach (Room room in Rooms)
                capacitySum += room.Capacity;
            return capacitySum;
        }

        
        #region additional getters

        /// <summary>
        /// Return all creatures in the Dungeon. The player is excluded.
        /// </summary>
        public List<Creature> Creatures { get; } = new List<Creature>();

        /// <summary>
        /// Return all items in this Dungeon. The items in the player's bag
        /// are excluded.
        /// </summary>
        public List<Item> Items { get; } = new List<Item>();

        #endregion

        /// <summary>
        /// Populate the dungeon with the specified number of monsters and items.
        /// They are dropped in random locations. Keep in mind that the number of
        /// monsters in a room should not exceed the room's capacity. There are also
        /// other constraints; see the Project Document.
        /// <para></para>
        /// Note that it is not always possible to populate the dungeon according to
        /// the specified parameters. E.g. in a dungeon with N rooms whose capacity
        /// are between 0 and k, it is definitely not possible to populate it with
        /// (N-2)*k monsters or more.
        /// The method returns true if it manages to populate the dungeon as specified,
        /// else it returns false.
        /// <para></para>
        /// If it fails to populate the dungeon, it returns false.
        /// </summary>
        public bool SeedMonstersAndItems(int numberOfMonster, int numberOfHealingPotion, int numberOfRagePotion)
        {
            /* The requirements don't necessarily have to be met to generate the dungeon.
             * Put in the best effort to generate a dungeon that satisfies the requirements.
             * If it violates any requirements then flag the boolean as false.
             * The goal is to always produce a dungeon that doesn't violate any of the requirements.
             * Adjust values where necessary to do so, but still flag as false.
             */
            bool satisfiedRequirements = true;

            #region Specific Requirements

            bool monsterAliveRequirement = true;
            bool roomCapacityNotViolatedRequirement = true;
            bool exitNeighborRoomsPopulatedCorrectlyRequirement = true;
            bool numberofRoomsWithoutItemRequirement = true;
            bool healingPotionAdjacencyRequirement = true;
            bool ragePotionsOnlyInLeavesRequirement = true;
            
            #endregion
            
            int nMon = numberOfMonster;
            int uniqueID = 1;
            int nHP = numberOfHealingPotion;
            int nRP = numberOfRagePotion;
            //This number indicates the amount of rooms that can contain an item, decrease when populating with items.
            int nRoomsWithItem = Rooms.Count / 2;
            int nSupposedRoomsWithItem = nRoomsWithItem;
            
            //Preliminary requirement checks; requirements b and f can be checked before generating.
            //The total capacity of the dungeon cannot contain the asked amount of monsters.
            if (nMon > TotalDungeonCapacity())
            {
                nMon = TotalDungeonCapacity();
                roomCapacityNotViolatedRequirement = false;
            }
            
            //Rage potions cannot generate in a linear dungeon.
            if (this.DungeonShape == DungeonShapeType.LINEARshape && nRP > 0)
            {
                nRP = 0;
                ragePotionsOnlyInLeavesRequirement = false;
            }

            //Populate dungeon with monsters.
            //Fill the neighbors of the exit room first to easily conform to requirement d.
            foreach (Room room in ExitRoom.Neighbors)
            {
                int monstersToAdd;
                if (nMon >= room.Capacity)
                {
                    monstersToAdd = room.Capacity;
                    nMon -= monstersToAdd;
                }
                else
                {
                    monstersToAdd = nMon;
                    nMon -= monstersToAdd;
                }

                for (int i = 0; i < monstersToAdd; i++)
                {
                    Monster m = new Monster("M" + uniqueID.ToString(), Creature.Stats.goblin);
                    room.Creatures.Add(m);
                    m.Location = room;
                    uniqueID++;
                }
            }
            
            //Now fill the rest of the rooms with the remaining monsters, skipping the start and exit rooms.
            while (nMon != 0)
            {
                foreach (Room room in Rooms)
                {
                    if (room == StartRoom || room == ExitRoom)
                        continue;
                    if (room.NumberOfMonsters < room.Capacity && nMon > 0)
                    {
                        Monster m = new Monster("M" + uniqueID.ToString(), Creature.Stats.goblin);
                        room.Creatures.Add(m);
                        m.Location = room;
                        uniqueID++;
                        nMon--;
                    }
                }
            }

            //Populate dungeon with items.
            
            //Check if there are any rooms eligible for rage potions.
            //If there are no rooms with a neighbor count of 1, then no rage potions can be seeded.
            bool dungeonHasRoomsWithNBCount1 = false;
            foreach (Room room in Rooms)
            {
                if (room == StartRoom || room == ExitRoom)
                    continue;
                if (room.Neighbors.Count == 1)
                {
                    dungeonHasRoomsWithNBCount1 = true;
                    break;
                }
            }

            if (dungeonHasRoomsWithNBCount1 == false && nRP > 0)
                ragePotionsOnlyInLeavesRequirement = false;
            
            //Then fill with Rage potions.
            while (nRP != 0 && dungeonHasRoomsWithNBCount1)
            {
                foreach (Room room in Rooms)
                {
                    if (room == StartRoom || room == ExitRoom)
                        continue;
                    
                    if (nRP > 0 && room.Neighbors.Count == 1 && (room.Items.Count > 0 || nRoomsWithItem > 0))
                    {
                        if (room.Items.Count == 0)
                            nRoomsWithItem--;
                        room.Items.Add(new RagePotion("R:" + uniqueID.ToString()));
                        nRP--;
                        uniqueID++;
                    }
                }
            }
            
            //lastly fill with HP potions.
            while (nHP != 0)
            {
                //double foreach, maybe not too pretty
                foreach (Room room in Rooms)
                {
                    if (room == StartRoom || room == ExitRoom)
                        continue;
                    
                    bool neighborHasHP = false;
                    foreach (Room neighbor in room.Neighbors)
                    {
                        if (neighbor.Items.Any(h => h is HealingPotion))
                            neighborHasHP = true;
                    }

                    if (nHP > 0 && !neighborHasHP && (room.Items.Count > 0 || nRoomsWithItem > 0))
                    {
                        if (room.Items.Count == 0)
                            nRoomsWithItem--;
                        room.Items.Add(new HealingPotion("H" + uniqueID.ToString(), 25));
                        nHP--;
                        uniqueID++;
                    }
                }
            }
            
            //!!afterwards, check if numberofRoomsWithoutItemRequirement isn't violated -> iteration over rooms with a count, compare at end
            
            //do requirement checks here.
            
            
            //check if any of the requirements were violated
            satisfiedRequirements = monsterAliveRequirement &&
                                    roomCapacityNotViolatedRequirement &&
                                    exitNeighborRoomsPopulatedCorrectlyRequirement &&
                                    numberofRoomsWithoutItemRequirement &&
                                    healingPotionAdjacencyRequirement &&
                                    ragePotionsOnlyInLeavesRequirement;

            foreach (Room room in Rooms)
            {
                foreach (Item i in room.Items)
                {
                    Items.Add(i);
                }
            }
            
            foreach (Room room in Rooms)
            {
                foreach (Creature c in room.Creatures)
                {
                    Creatures.Add(c);
                }
            }
            return satisfiedRequirements;
        }
    }

    [Serializable()]
    public enum DungeonShapeType
    {
        LINEARshape, 
        TREEshape,
        RANDOMshape
    }
    
    /// <summary>
    /// Representing different types of rooms.
    /// </summary>
    public enum RoomType
    {
        STARTroom,  // the starting room of the player. 
        EXITroom,   // representing the player's final destination.
        ORDINARYroom  // the type of the rest of the rooms. 
    }

    /// <summary>
    /// Representing a room in a dungeon.
    /// </summary>
    public class Room : GameEntity
    {

        #region fields and properties

        /// <summary>
        /// The type of this node: either start-node, exit-node, or common-node.
        /// </summary>
        public RoomType RoomType { get; }

        /// <summary>
        /// The number of monsters in this room cannot exceed this capacity.
        /// </summary>
        public int Capacity { get; set; }
        
        /// <summary>
        /// Neighbors are nodes that are considered connected to this node.
        /// The connection is bidirectional. If u is in this.neighbors of this room,
        /// you have to make sure that this room is also in u.neighbors.
        /// </summary>
        public List<Room> Neighbors { get; } = new List<Room>();

        /// <summary>
        /// All creatures, excluding the players, which are currently in this room.
        /// </summary>
        public List<Creature> Creatures { get;  } = new List<Creature>();

        /// <summary>
        /// All items, excluding those in the player's bag, which are currently in this room.
        /// </summary>
        public List<Item> Items { get; } = new List<Item>();
        
        #endregion
        
        
        public Room(string uniqueId, RoomType roomTy, int capacity) : base(uniqueId)
        {
            RoomType = roomTy;
            Capacity = capacity;
        }

        #region additional getters
       
        /// <summary>
        /// The number of monsters in this room. The player does not count as a monster.
        /// </summary>
        public int NumberOfMonsters => Creatures.Count(c => c is Monster);

        #endregion

        /// <summary>
        /// To add the given room as a neighbor of this room.
        /// </summary>
        public void Connect(Room r)
        {
            Neighbors.Add(r); r.Neighbors.Add(this);
        }

        /// <summary>
        /// To disconnect the given room. That is, the room r will no longer be a
        /// neighbor of this room.
        /// </summary>
        public void Disconnect(Room r)
        {
            Neighbors.Remove(r); r.Neighbors.Remove(this);
        }

        /// <summary>
        /// return the set of all rooms which are reachable from this room.
        /// </summary>
        public List<Room> ReachableRooms()
        {
            Room x = this;
            var seen = new List<Room>();
            var todo = new List<Room>();
            todo.Add(x);
            while (todo.Count > 0)
            {
                x = todo[0] ; todo.RemoveAt(0) ;
                seen.Add(x);
                foreach (Room y in x.Neighbors)
                {
                    if (seen.Contains(y) 
                        || todo.Contains(y))
                        continue;
                    todo.Add(y);
                }
            }
            return seen;
        }

        /// <summary>
        /// Check if the given room is reachable from this room.
        /// </summary>
        public bool CanReach(Room r)
        {
            return ReachableRooms().Contains(r); // not the most efficient way of checking it btw
        }
    }



}
