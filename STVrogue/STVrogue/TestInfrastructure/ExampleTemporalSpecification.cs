﻿using System;
using System.Linq;
using STVrogue.GameLogic;

namespace STVrogue.TestInfrastructure
{
    public class ExampleTemporalSpecification
    {
        /// <summary>
        /// The player's hit point is never negative.
        /// </summary>
        static public TemporalProperty<Game> example1 = new Always<Game>(G=>G.Player.Hp >= 0);
        
        /// <summary>
        /// The player's kill point is never negative.
        /// </summary>
        static public TemporalProperty<Game> example2 = new Always<Game>(G=>G.Player.Kp >= 0);
        
        /// <summary>
        /// The player's kill point never decreases.
        /// </summary>
        static public TemporalProperty<Game> example3 
            = new Always<Game>(G =>G.Player.Kp, (before,now) => before >= now) ;
        
        
        

        static public TemporalProperty<Game> SARoomCap =
            new Always<Game>(G => G.Dungeon.Rooms.TrueForAll(d => d.Capacity >= d.NumberOfMonsters));

        static public TemporalProperty<Game> SKP = new Always<Game>(G => G.Player.Kp + nrOfMonsters(G) == G.Config.initialNumberOfMonsters);

        private static int nrOfMonsters(Game game)
        {
            int counter = 0;
            foreach (Room r in game.Dungeon.Rooms)
            {
                counter += r.NumberOfMonsters;
            }
            return counter;
        }

        static public TemporalProperty<Game> SEWin = TemporalProperty<Game>.Eventually(G => G.Player.Location.RoomType == RoomType.EXITroom);
        static public TemporalProperty<Game> SELose = TemporalProperty<Game>.Eventually(G => !G.Player.Alive);
        static public TemporalProperty<Game> SEWinLose = TemporalProperty<Game>.Or(SEWin, SELose);
        
        static public TemporalProperty<Game> GotHit = TemporalProperty<Game>.Eventually(G => G.Player.Hp < G.Player.HpMax);
        static public TemporalProperty<Game> FoundMon = TemporalProperty<Game>.Eventually(G => G.Player.Location.NumberOfMonsters > 0);
        static public TemporalProperty<Game> HitAndMon = TemporalProperty<Game>.And(GotHit, FoundMon);

        static public TemporalProperty<Game> AlwaysMax = new Always<Game>(G => G.Player.Hp == G.Player.HpMax);
        static public TemporalProperty<Game> SCAHpDrop1 = TemporalProperty<Game>.Or(AlwaysMax, HitAndMon);

        
        static public TemporalProperty<Game> TenTurn = TemporalProperty<Game>.Eventually(G => G.TurnNumber > 10);
        static public TemporalProperty<Game> NoMon = TemporalProperty<Game>.Always(G => G.Player.Location.NumberOfMonsters == 0);
        static public TemporalProperty<Game> WithMon = TemporalProperty<Game>.Eventually(G => G.Player.Location.NumberOfMonsters > 0);
        static public TemporalProperty<Game> TenTurnNoMon = TemporalProperty<Game>.And(TenTurn, NoMon);
        
        //static public TemporalProperty<Game> SCAHpDropHelp = TemporalProperty<Game>.And(AlwaysMax, TenTurnNoMon);
        //static public TemporalProperty<Game> SCAHpDrop2 = TemporalProperty<Game>.Or(SCAHpDropHelp, WithMon);
        static public TemporalProperty<Game> SCAHpDrop2 = TemporalProperty<Game>.And(AlwaysMax, TenTurnNoMon);
        
        static public TemporalProperty<Game> SANitem = new Always<Game>(G => G.Player.Bag.Count + G.Dungeon.Items.Count <= G.Config.initialNumberOfHealingPots + G.Config.initialNumberOfRagePots);
        
        
    }
    
    
    
}
