using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using NUnit.Framework;
using STVrogue.GameLogic;

namespace NUnitTests
{
    [TestFixture]
    public class Test_GameConfiguration
    {
        [Test, Pairwise]
        [Description("Test that on bad inputs the constructor does throw an exception")]
        public void fullCombinatoricTest_execeptionCase_GameConfiguration(
            [Values(DifficultyMode.NORMALmode, DifficultyMode.ELITEmode, DifficultyMode.NEWBIEmode)] DifficultyMode difmode,
            [Values(DungeonShapeType.TREEshape, DungeonShapeType.LINEARshape, DungeonShapeType.RANDOMshape)] DungeonShapeType shape,
            [Values(-1,0,1,2,3,4,5,6,7,8,9,10)] int inimon,
            [Values(0,1,2,3,4,5,6,7,8,9,10)] int maxrc,
            [Values(0,1,2,3,4,5,6,7,8,9,10)] int nrrooms,
            [Values(0,1,2,3,4,5,6,7,8,9,10)] int inihp,
            [Values(0,1,2,3,4,5,6,7,8,9,10)] int inirp
        )
        {
            GameConfiguration conf = new GameConfiguration();
            conf.difficultyMode = difmode;
            conf.dungeonShape = shape;
            conf.initialNumberOfMonsters = inimon;
            conf.maxRoomCapacity = maxrc;
            conf.numberOfRooms = nrrooms;
            conf.initialNumberOfHealingPots = inihp;
            conf.initialNumberOfRagePots = inirp;
            
            //a few condition under which the configuration will NEVER succeed to test if the exception will be thrown
            if (nrrooms < 3)
                Assert.Throws<ArgumentException>(() => new Game(conf));
            if (inimon > 0 && maxrc == 0)
                Assert.Throws<ArgumentException>(() => new Game(conf));
            if (shape == DungeonShapeType.LINEARshape && inirp > 0)
                Assert.Throws<ArgumentException>(() => new Game(conf));
            
            //a condition under which the configuration will ALWAYS succeed to test if the configuration is consistent
            if (nrrooms > 3 && inihp == 0 && inirp == 0 && inimon == 0)
            {
                Game gme = new Game(conf);
                Assert.AreEqual(conf.difficultyMode, gme.Config.difficultyMode);
                Assert.AreEqual(conf.dungeonShape, gme.Config.dungeonShape);
                Assert.AreEqual(conf.initialNumberOfMonsters, gme.Config.initialNumberOfMonsters);
                Assert.AreEqual(conf.initialNumberOfMonsters, gme.Dungeon.Creatures.Count);
                Assert.AreEqual(conf.maxRoomCapacity, gme.Config.maxRoomCapacity);
                Assert.AreEqual(conf.numberOfRooms, gme.Config.numberOfRooms);
                Assert.AreEqual(conf.numberOfRooms, gme.Dungeon.Rooms.Count);
                Assert.AreEqual(conf.initialNumberOfHealingPots, gme.Dungeon.Items.Count(x => x is HealingPotion));
                Assert.AreEqual(conf.initialNumberOfHealingPots, gme.Config.initialNumberOfHealingPots);
                Assert.AreEqual(conf.initialNumberOfRagePots, gme.Dungeon.Items.Count(x => x is RagePotion));
                Assert.AreEqual(conf.initialNumberOfRagePots, gme.Config.initialNumberOfRagePots);

                Assert.AreEqual(gme.Player.Location, gme.Dungeon.StartRoom);
                Assert.AreEqual(gme.Player.Hp, gme.Player.HpMax);

            }
            
            Assert.DoesNotThrow(() => conf.ToString());
        }
        
    }
}