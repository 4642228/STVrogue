using NUnit.Framework;
using STVrogue.GameLogic;
using System;
using System.Diagnostics;
using System.Linq;
using STVrogue;
using STVrogue.Utils;

namespace NUnitTests
{
    [TestFixture]
    public class Test_Flee
    {
        public Game game;
        private GameConfiguration config = new GameConfiguration();
        
        [SetUp]
        public void Init()
        {
            STVControlledRandom.Reset();
            
            config.dungeonShape = DungeonShapeType.LINEARshape;
            config.maxRoomCapacity = 3;
            config.difficultyMode = DifficultyMode.NEWBIEmode;
            config.numberOfRooms = 3;
            config.initialNumberOfMonsters = 0;
            config.initialNumberOfHealingPots = 0;
            config.initialNumberOfRagePots = 0;
            
        }

        [Test]
        public void PlayerFleeNewbie()
        {
            game = new Game(config);
            Command c = new Command(CommandType.FLEE, new string[]{});

            var start = game.Dungeon.StartRoom.Neighbors.First();
            game.Player.Location = start;
            game.Update(c);
            bool fled = start != game.Player.Location;
            
            Assert.Multiple(() =>
            {
                // b. The player can not flee to the exit room
                bool b = fled && game.Player.Location == game.Dungeon.ExitRoom;
                Assert.IsFalse(b);

                // c. In newbie mode, the player can always flee
                Assert.IsTrue(fled);
            });
        }
        
        [Test]
        public void PlayerFleeNormal()
        {
            config.difficultyMode = DifficultyMode.NORMALmode;
            game = new Game(config);
            Command c = new Command(CommandType.FLEE, new string[]{});

            var start = game.Dungeon.StartRoom.Neighbors.First();
            game.Player.Location = start;
            game.Update(c);
            bool fled = start != game.Player.Location;
            
            Assert.IsTrue(fled);
        }
        
        [Test]
        public void PlayerFleeNormalHealPotion()
        {
            config.difficultyMode = DifficultyMode.NORMALmode;
            game = new Game(config);
            Command c = new Command(CommandType.FLEE, new string[]{});

            var start = game.Dungeon.StartRoom.Neighbors.First();
            game.Player.Location = start;
            game.Player.Bag.Add(new HealingPotion("Healing Potion", 1));
            game.Update(new Command(CommandType.USE, "Healing Potion"));
            game.Update(c);
            bool fled = start != game.Player.Location;
            
            // d. In normal mode, the player cannot flee if in the previous turn it uses a potion.
            Assert.IsFalse(fled);
        }
        
        [Test]
        public void PlayerFleeElite()
        {
            config.difficultyMode = DifficultyMode.ELITEmode;
            game = new Game(config);
            Command c = new Command(CommandType.FLEE, new string[]{});

            var start = game.Dungeon.StartRoom.Neighbors.First();
            game.Player.Location = start;
            game.Update(c);
            bool fled = start != game.Player.Location;
            
            Assert.IsTrue(fled);
        }
        
        [Test]
        public void PlayerFleeEliteHealPotion()
        {
            config.difficultyMode = DifficultyMode.ELITEmode;
            game = new Game(config);
            Command c = new Command(CommandType.FLEE, new string[]{});

            var start = game.Dungeon.StartRoom.Neighbors.First();
            game.Player.Location = start;
            game.Player.Bag.Add(new HealingPotion("Healing Potion", 1));
            game.Update(new Command(CommandType.USE, "Healing Potion"));
            game.Update(c);
            bool fled = start != game.Player.Location;
            Assert.False(fled);
        }
        
        [Test]
        public void PlayerFleeEliteRagePotion()
        {
            config.difficultyMode = DifficultyMode.ELITEmode;
            game = new Game(config);
            Command c = new Command(CommandType.FLEE, new string[]{});

            var start = game.Dungeon.StartRoom.Neighbors.First();
            game.Player.Location = start;
            game.Player.Bag.Add(new RagePotion("Rage Potion"));
            game.Update(new Command(CommandType.USE, "Rage Potion"));
            game.Update(new Command(CommandType.DoNOTHING));
            game.Update(c);
            bool fled = start != game.Player.Location;
            Assert.False(fled);
        }
        
        [Test]
        public void MonsterFleeFailed()
        {
            config.difficultyMode = DifficultyMode.NORMALmode;
            game = new Game(config);
            
            var start = game.Dungeon.StartRoom.Neighbors.First();
            var monster = new Monster("1", Creature.Stats.goblin);
            monster.Location = start;
            start.Creatures.Add(monster);
            game.Flee(monster);
            bool fled = start != monster.Location;
            
            // No space to flee 
            Assert.False(fled);
        }
        
        [Test]
        public void MonsterFleeSucceeded()
        {
            config.difficultyMode = DifficultyMode.NORMALmode;
            config.numberOfRooms = 4;
            game = new Game(config);
            
            var start = game.Dungeon.StartRoom.Neighbors.First();
            var monster = new Monster("1", Creature.Stats.goblin);
            monster.Location = start;
            start.Creatures.Add(monster);
            game.Flee(monster);
            bool fled = start != monster.Location;
            
            // Extra room allows for monster to flee 
            Assert.True(fled);
        }
    }
}