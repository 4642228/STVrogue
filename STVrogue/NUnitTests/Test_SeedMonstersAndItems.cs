using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using NUnit.Framework;
using STVrogue.GameLogic;
using STVrogue.Utils;

namespace NUnitTests
{
    [TestFixture]
    public class Test_SeedMonstersAndItems
    {
        [SetUp]
        public void setup()
        {
            STVControlledRandom.Reset();
        }

        [DatapointSource] public int[] Values = new int[] {0, 3, 7, 15};
        
        [Theory]
        public void CorrectSeedingTheory(int healPot, int ragePot, int monsters, int capacity, int rooms)
        {
            Assume.That(capacity > 0 && rooms > 2);
            Dungeon testDun = new Dungeon(DungeonShapeType.RANDOMshape, rooms, capacity);
            bool smi = testDun.SeedMonstersAndItems(monsters, healPot, ragePot);
            Assume.That(smi);

            Assert.Multiple(() =>
            {
                int emptyRooms = 0, maxMonInOrdinaryRoom = 0;

                foreach (Room room in testDun.Rooms)
                {
                    if (room.Items.Count == 0)
                        emptyRooms++;

                    if (room.Id == "start" || room.Id == "end")
                        continue;

                    if (room.NumberOfMonsters > maxMonInOrdinaryRoom && !testDun.ExitRoom.Neighbors.Contains(room))
                        maxMonInOrdinaryRoom = room.NumberOfMonsters;

                    if (room.Items.Any(i => i is HealingPotion))
                    {
                        foreach (Room nb in room.Neighbors)
                        {
                            Assert.False(nb.Items.Any(u => u is HealingPotion), "A neighbor of a room with HealingPot also has a HealingPot.");
                        }
                    }

                    if (room.Creatures.Count > 0)
                    {
                        foreach (Creature mon in room.Creatures)
                        {
                            Assert.That(mon.Alive && mon.AttackRating > 0 && mon.Hp > 0, "Monster does not fulfill requirements.");
                        }

                        Assert.That(room.Creatures.Count <= room.Capacity, "More monsters in a room than it's capacity.");
                    }

                    if (room.Items.Any(u => u is RagePotion))
                        Assert.That(room.Neighbors.Count == 1, "RagePot in a room that is no leaf.");
                }
                
                Assert.That(emptyRooms >= testDun.Rooms.Count / 2, "There are not enough empty rooms in the dungeon.");

                foreach (Room neigh in testDun.ExitRoom.Neighbors)
                {
                    if (neigh.RoomType != RoomType.STARTroom)
                    {
                        Assert.That(neigh.NumberOfMonsters >= maxMonInOrdinaryRoom, "A neighbor of the exitroom does not have enough monsters.");
                    }
                }
                
                Assert.That(healPot + ragePot == testDun.Items.Count);
            });
        }
    }
}