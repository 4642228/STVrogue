using System.Linq;
using NUnit.Framework;
using STVrogue;
using STVrogue.GameLogic;

namespace NUnitTests
{
    [TestFixture]
    public class Test_Update
    {
        private GameConfiguration conf = new GameConfiguration()
        {
            rndSeed = 1, 
            numberOfRooms = 10, 
            maxRoomCapacity = 5, 
            initialNumberOfMonsters = 15, 
            initialNumberOfHealingPots = 3, 
            initialNumberOfRagePots = 3, 
            difficultyMode = DifficultyMode.NORMALmode, 
            dungeonShape = DungeonShapeType.TREEshape
        };
        
        [Test()]
        public void test_DoNothing()
        {
            Game g = new Game(conf){GameConsole = new GameConsole()};
            Command c = new Command(CommandType.DoNOTHING, new string[]{});
            
            g.Update(c);
            
            Assert.IsTrue(g.TurnNumber == 1);
        }
        
        [Test()]
        public void test_Move()
        {
            Game g = new Game(conf){GameConsole = new GameConsole()};
            Room target = g.Player.Location.Neighbors[0];
            Command c = new Command(CommandType.MOVE, new string[]{target.Id});
            
            g.Update(c);
            
            Assert.IsTrue(g.Player.Location == target);
        }

        [Test()]
        public void test_Attack()
        {
            Game g = new Game(conf){GameConsole = new GameConsole()};
            Room monsterRoom = g.Dungeon.Rooms.Find((Room r) => r.NumberOfMonsters > 0);
            Monster m = (Monster)monsterRoom.Creatures[0];
            g.Player.Location = monsterRoom;
            Command c = new Command(CommandType.ATTACK, new string[]{m.Id});
            
            g.Update(c);
            
            Assert.IsTrue(m.Hp == m.HpMax - g.Player.AttackRating);
        }
        
        [Test()]
        public void test_Pickup()
        {
            Game g = new Game(conf){GameConsole = new GameConsole()};
            HealingPotion healing = new HealingPotion("test_healing", 10);
            RagePotion rage = new RagePotion("test_rage");
            g.Player.Location.Items.Add(healing);
            g.Player.Location.Items.Add(rage);
            Command c = new Command(CommandType.PICKUP, new string[]{});
            
            g.Update(c);
            
            Assert.IsTrue(g.Player.Bag.Contains(healing));
            Assert.IsFalse(g.Player.Location.Items.Contains(healing));
            Assert.IsTrue(g.Player.Bag.Contains(rage));
            Assert.IsFalse(g.Player.Location.Items.Contains(rage));
        }
        
        [Test()]
        public void test_UseItemHealing()
        {
            Game g = new Game(conf){GameConsole = new GameConsole()};
            g.Player.Hp -= g.Player.HpMax / 2;
            HealingPotion healing = new HealingPotion("test_healing", 1);
            g.Player.Bag.Add(healing);
            Command c = new Command(CommandType.USE, new string[]{healing.Id});
            int oldHp = g.Player.Hp;
            
            g.Update(c);
            
            Assert.IsTrue(g.Player.Hp == int.Min(oldHp + healing.HealValue, g.Player.HpMax));
            Assert.IsFalse(g.Player.Bag.Contains(healing));
        }
        
        [Test()]
        public void test_UseItemRage()
        {
            Game g = new Game(conf){GameConsole = new GameConsole()};
            RagePotion rage = new RagePotion("test_rage");
            g.Player.Bag.Add(rage);
            Command c = new Command(CommandType.USE, new string[]{rage.Id});
            
            g.Update(c);
            
            Assert.IsTrue(g.Player.Enraged);
            Assert.IsFalse(g.Player.Bag.Contains(rage));
        }
    }
}