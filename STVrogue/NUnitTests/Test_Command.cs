using NUnit.Framework;
using STVrogue.GameLogic;

namespace NUnitTests
{
    [TestFixture]
    public class Test_Command
    {
        [Test()]
        public void test_ToString(
            [Values(CommandType.DoNOTHING, 
                CommandType.FLEE, 
                CommandType.MOVE, 
                CommandType.USE, 
                CommandType.ATTACK, 
                CommandType.PICKUP)] 
            CommandType ct)
        {
            string[] args = new string[] { "auniqueid", "anotheruniqueid" };
            Command c = new Command(ct, args);

            string s = null;
            //Assert the function returns a valid string
            Assert.DoesNotThrow(() => s = c.ToString());
            Assert.IsTrue(s != null);
            Assert.IsTrue(s.Length > 0);
            //Assert input is used
            Assert.IsTrue(c.Args == args);
            Assert.IsTrue(c.Name == ct);
        }
    }
}