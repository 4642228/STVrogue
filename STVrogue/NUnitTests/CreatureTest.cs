using System;
using NUnit.Framework;
using STVrogue.GameLogic;

namespace NUnitTests
{
    [TestFixture]
    public class CreatureTest
    {
        #region MonsterMoveTests
        
        [Test, Description("A valid monster movement test")]
        public void ValidMonsterMove()
        {
            Room at = new Room("at", RoomType.ORDINARYroom, 1);
            Room to = new Room("to", RoomType.ORDINARYroom, 1);
            Monster m = new Monster("monster", Creature.Stats.goblin);
            
            at.Connect(to);
            at.Creatures.Add(m);
            m.Location = at;
            
            m.Move(to);
            
            Assert.IsTrue(m.Location == to);
            Assert.IsFalse(at.Creatures.Contains(m));
            Assert.IsTrue(to.Creatures.Contains(m));
        }
        
        [Test, Description("Invalid monster movement into a full room test")]
        public void FullRoomMonsterMove()
        {
            Room at = new Room("at", RoomType.ORDINARYroom, 1);
            Room to = new Room("to", RoomType.ORDINARYroom, 0);
            Monster m = new Monster("monster", Creature.Stats.goblin);
            
            at.Connect(to);
            at.Creatures.Add(m);
            m.Location = at;

            Assert.Throws<ArgumentException>(() => m.Move(to));
        }
        
        [Test, Description("Invalid monster movement into a disconnected room test")]
        public void DisconnectedRoomMonsterMove()
        {
            Room at = new Room("at", RoomType.ORDINARYroom, 1);
            Room to = new Room("to", RoomType.ORDINARYroom, 1);
            Monster m = new Monster("monster", Creature.Stats.goblin);
            
            at.Creatures.Add(m);
            m.Location = at;

            Assert.Throws<ArgumentException>(() => m.Move(to));
        }
        
        #endregion
        
        #region PlayerMoveTests
        
        [Test, Description("Valid player movement test")]
        public void ValidPlayerMove()
        {
            Room at = new Room("at", RoomType.ORDINARYroom, 1);
            Room to = new Room("to", RoomType.ORDINARYroom, 1);
            Player p = new Player("player");
                
            at.Connect(to);
            p.Location = at;
            
            p.Move(to);
            
            Assert.IsTrue(p.Location == to);
        }
        
        [Test, Description("Invalid player movement into a disconnected room test")]
        public void DisconnectedPlayerMove()
        {
            Room at = new Room("at", RoomType.ORDINARYroom, 1);
            Room to = new Room("to", RoomType.ORDINARYroom, 1);
            Player p = new Player("player");

            p.Location = at;

            Assert.Throws<ArgumentException>(() => p.Move(to));
        }
        
        #endregion
        
        #region CreatureAttackTests

        [Test, Description("Test player attacking monster test")]
        public void PlayerOnMonsterCreatureAttack()
        {
            Player p = new Player("player");
            Monster m = new Monster("monster", Creature.Stats.goblin);
            Room r = new Room("at", RoomType.ORDINARYroom, 2);

            p.Location = r;
            m.Location = r;
            r.Creatures.Add(m);
            
            p.Attack(m);
            m.Attack(p);
            
            Assert.IsTrue(m.Hp == m.HpMax - p.AttackRating);
            Assert.IsTrue(p.Hp == p.HpMax - m.AttackRating);
            
            if (m.HpMax > p.AttackRating)
                Assert.IsTrue(p.Kp == 0);
        }

        [Test, Description("Test player attacking and killing monster test")]
        public void PlayerOnMonsterAttackAndKillCreatureAttack()
        {
            Player p = new Player("player");
            Monster m = new Monster("monster", Creature.Stats.goblin);
            Room r = new Room("at", RoomType.ORDINARYroom, 2);
            
            p.Location = r;
            m.Location = r;
            r.Creatures.Add(m);
            p.AttackRating = 1;
            m.Hp = 1;
            
            p.Attack(m);

            Assert.IsTrue(p.Kp == 1);
        }

        [Test, Description("Player attacks monster in a disconected room test")]
        public void PlayerOnDisconectedMonsterCreatureAttack()
        {
            Player p = new Player("player");
            Monster m = new Monster("monster", Creature.Stats.goblin);
            Room pRoom = new Room("pRoom", RoomType.ORDINARYroom, 2);
            Room mRoom = new Room("mRoom", RoomType.ORDINARYroom, 2);

            p.Location = pRoom;
            m.Location = mRoom;
            mRoom.Creatures.Add(m);

            Assert.Throws<ArgumentException>(() => p.Attack(m));
        }
        
        [Test, Description("Monster attacks another monster test")]
        public void MonsterOnMonsterCreatureAttack()
        {
            Monster m1 = new Monster("m1", Creature.Stats.goblin);
            Monster m2 = new Monster("m2", Creature.Stats.goblin);
            Room r = new Room("at", RoomType.ORDINARYroom, 2);

            m1.Location = r;
            m2.Location = r;
            r.Creatures.Add(m1);
            r.Creatures.Add(m2);

            Assert.Throws<ArgumentException>(() => m1.Attack(m2));
            Assert.Throws<ArgumentException>(() => m2.Attack(m1));
        }
        
        [Test, Description("Player attacks itself test")]
        public void SelfAttackCreatureAttack()
        {
            Player p = new Player("player");
            Room r = new Room("at", RoomType.ORDINARYroom, 2);

            p.Location = r;

            Assert.Throws<ArgumentException>(() => p.Attack(p));
        }
        
        #endregion
    }
}