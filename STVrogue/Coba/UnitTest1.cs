using System;
using System.Diagnostics;
using NUnit.Framework;

namespace Coba
{

    class Coba1
    {
        public string Name { get; }
        
        public Coba1(string name)
        {
            Name = name;
        }
    }
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            int x = 10;
            Console.WriteLine(("Coba console write..."));
            Debug.WriteLine("Coba debug write...");
            x = x + 1;
            Assert.Pass();
        }
        
        [Test]
        public void Test2()
        {
            Coba1 coba1 = new Coba1("Haha");
            Assert.That(coba1.Name == "Haha"); 
            Console.WriteLine(">>> " + coba1.Name);
        }
    }
}