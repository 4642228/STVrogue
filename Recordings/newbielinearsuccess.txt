4354:7:3:LINEARshape:9:3:0:NORMALmode
MsgIn
u
 
p
a
f
m
R1
p
u
H10
a
M4
a
M4
a
M8
a
M6
a
M6
a
M8
m
R2
a
M7
a
M7
a
M5
a
M5
m
R3
p
a
M3
a
M3
a
M9
a
M9
a
M1
a
M1
m
R2
p

u
H11
m
R3
m
R4
a
M2
a
M2
m
R5
m
E
MsgOut
 _______ _________            _______  _______  _______           _______ 
(  ____ \\__   __/|\     /|  (  ____ )(  ___  )(  ____ \|\     /|(  ____ \
| (    \/   ) (   | )   ( |  | (    )|| (   ) || (    \/| )   ( || (    \/
| (_____    | |   | |   | |  | (____)|| |   | || |      | |   | || (__    
(_____  )   | |   ( (   ) )  |     __)| |   | || | ____ | |   | ||  __)   
      ) |   | |    \ \_/ /   | (\ (   | |   | || | \_  )| |   | || (      
/\____) |   | |     \   /    | ) \ \__| (___) || (___) || (___) || (____/\
\_______)   )_(      \_/     |/   \__/(_______)(_______)(_______)(_______/
Welcome stranger...

TURN 0
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 0
You are currently in room: S
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.
You have no Items to use!

TURN 0
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 0
You are currently in room: S
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.

** player DoNOTHING()
      Lazy. Start working!
Cannot flee.

TURN 1
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 0
You are currently in room: S
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.

** player PICKUP()
      You try to pick up some Items!
there are no items to pick up!

TURN 1
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 0
You are currently in room: S
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.
There are no monsters to attack!

TURN 1
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 0
You are currently in room: S
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.

** player FLEE()
      We knew you are a coward.
Cannot flee.

TURN 1
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 0
You are currently in room: S
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.
      Which Room do you want to move to? (give the id number)
	 ID: R1

** player MOVE(R1)
      You try to move to the next room
You successfully moved!

TURN 2
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 0
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
The current monsters in this room are:
	 Monster: ID:M4
The current items in this room are:
	 Healingpotion: ID:H10
There are no items in your bag.

** player PICKUP()
      You try to pick up some Items!
You got attacked by goblin!

TURN 3
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 99, kill-counts: 0
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
The current monsters in this room are:
	 Monster: ID:M4
	 Monster: ID:M8
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H10

Which Item do you want to use? (give the id number)
	 ID: H10

** player USE(H10)
Cannot flee.

TURN 4
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 0
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
The current monsters in this room are:
	 Monster: ID:M4
	 Monster: ID:M8
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M4 HP: 10/10
	 ID: M8 HP: 10/10

** player ATTACK(M4)
      Clang! Wooosh. WHACK!
You successfully attack goblin!
You got attacked by goblin!
You got attacked by goblin!

TURN 5
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 98, kill-counts: 0
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
The current monsters in this room are:
	 Monster: ID:M4
	 Monster: ID:M8
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M4 HP: 5/10
	 ID: M8 HP: 10/10

** player ATTACK(M4)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 6
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 98, kill-counts: 1
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
The current monsters in this room are:
	 Monster: ID:M8
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M8 HP: 10/10

** player ATTACK(M8)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 7
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 98, kill-counts: 1
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
The current monsters in this room are:
	 Monster: ID:M6
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M6 HP: 10/10

** player ATTACK(M6)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 8
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 98, kill-counts: 1
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
The current monsters in this room are:
	 Monster: ID:M6
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M6 HP: 5/10

** player ATTACK(M6)
      Clang! Wooosh. WHACK!
You successfully attack goblin!
Cannot flee.

TURN 9
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 98, kill-counts: 2
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
The current monsters in this room are:
	 Monster: ID:M8
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M8 HP: 5/10

** player ATTACK(M8)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 10
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 98, kill-counts: 3
You are currently in room: R1
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:S
	 Room: Id:R2
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.
      Which Room do you want to move to? (give the id number)
	 ID: S
	 ID: R2

** player MOVE(R2)
      You try to move to the next room
You successfully moved!
Cannot flee.
You got attacked by goblin!

TURN 11
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 97, kill-counts: 3
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
The current monsters in this room are:
	 Monster: ID:M7
	 Monster: ID:M5
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M7 HP: 10/10
	 ID: M5 HP: 10/10

** player ATTACK(M7)
      Clang! Wooosh. WHACK!
You successfully attack goblin!
You got attacked by goblin!

TURN 12
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 96, kill-counts: 3
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
The current monsters in this room are:
	 Monster: ID:M7
	 Monster: ID:M5
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M7 HP: 5/10
	 ID: M5 HP: 10/10

** player ATTACK(M7)
      Clang! Wooosh. WHACK!
You successfully attack goblin!
You got attacked by goblin!

TURN 13
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 95, kill-counts: 4
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
The current monsters in this room are:
	 Monster: ID:M5
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M5 HP: 10/10

** player ATTACK(M5)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 14
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 95, kill-counts: 4
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
The current monsters in this room are:
	 Monster: ID:M5
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M5 HP: 5/10

** player ATTACK(M5)
      Clang! Wooosh. WHACK!
You successfully attack goblin!
Cannot flee.

TURN 15
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 95, kill-counts: 5
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.
      Which Room do you want to move to? (give the id number)
	 ID: R1
	 ID: R3

** player MOVE(R3)
      You try to move to the next room
You successfully moved!
Cannot flee.

TURN 16
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 95, kill-counts: 5
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
The current monsters in this room are:
	 Monster: ID:M3
The current items in this room are:
	 Healingpotion: ID:H11
There are no items in your bag.

** player PICKUP()
      You try to pick up some Items!
You got attacked by goblin!

TURN 17
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 94, kill-counts: 5
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
The current monsters in this room are:
	 Monster: ID:M3
	 Monster: ID:M9
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

Which Monster do you want to attack? (give the id number)
	 ID: M3 HP: 10/10
	 ID: M9 HP: 10/10

** player ATTACK(M3)
      Clang! Wooosh. WHACK!
You successfully attack goblin!
You got attacked by goblin!
You got attacked by goblin!

TURN 18
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 92, kill-counts: 5
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
The current monsters in this room are:
	 Monster: ID:M3
	 Monster: ID:M9
	 Monster: ID:M1
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

Which Monster do you want to attack? (give the id number)
	 ID: M3 HP: 5/10
	 ID: M9 HP: 10/10
	 ID: M1 HP: 10/10

** player ATTACK(M3)
      Clang! Wooosh. WHACK!
You successfully attack goblin!
You got attacked by goblin!

TURN 19
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 91, kill-counts: 6
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
The current monsters in this room are:
	 Monster: ID:M9
	 Monster: ID:M1
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

Which Monster do you want to attack? (give the id number)
	 ID: M9 HP: 10/10
	 ID: M1 HP: 10/10

** player ATTACK(M9)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 20
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 91, kill-counts: 6
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
The current monsters in this room are:
	 Monster: ID:M9
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

Which Monster do you want to attack? (give the id number)
	 ID: M9 HP: 5/10

** player ATTACK(M9)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 21
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 91, kill-counts: 7
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
The current monsters in this room are:
	 Monster: ID:M1
	 Monster: ID:M2
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

Which Monster do you want to attack? (give the id number)
	 ID: M1 HP: 10/10
	 ID: M2 HP: 10/10

** player ATTACK(M1)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 22
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 91, kill-counts: 7
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
The current monsters in this room are:
	 Monster: ID:M1
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

Which Monster do you want to attack? (give the id number)
	 ID: M1 HP: 5/10

** player ATTACK(M1)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 23
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 91, kill-counts: 8
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
There are no monsters in this room.
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11
      Which Room do you want to move to? (give the id number)
	 ID: R2
	 ID: R4

** player MOVE(R2)
      You try to move to the next room
You successfully moved!

TURN 24
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 91, kill-counts: 8
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
There are no monsters in this room.
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

** player PICKUP()
      You try to pick up some Items!
there are no items to pick up!

TURN 24
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 91, kill-counts: 8
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
There are no monsters in this room.
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

TURN 24
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 91, kill-counts: 8
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
There are no monsters in this room.
There are no items in this room.
The current items in your bag are:
	 Healingpotion: ID:H11

Which Item do you want to use? (give the id number)
	 ID: H11

** player USE(H11)

TURN 25
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 8
You are currently in room: R2
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R1
	 Room: Id:R3
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.
      Which Room do you want to move to? (give the id number)
	 ID: R1
	 ID: R3

** player MOVE(R3)
      You try to move to the next room
You successfully moved!

TURN 26
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 8
You are currently in room: R3
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R2
	 Room: Id:R4
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.
      Which Room do you want to move to? (give the id number)
	 ID: R2
	 ID: R4

** player MOVE(R4)
      You try to move to the next room
You successfully moved!

TURN 27
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 100, kill-counts: 8
You are currently in room: R4
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R3
	 Room: Id:R5
The current monsters in this room are:
	 Monster: ID:M2
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M2 HP: 10/10

** player ATTACK(M2)
      Clang! Wooosh. WHACK!
You successfully attack goblin!
You got attacked by goblin!

TURN 28
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 99, kill-counts: 8
You are currently in room: R4
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R3
	 Room: Id:R5
The current monsters in this room are:
	 Monster: ID:M2
There are no items in this room.
There are no items in your bag.

Which Monster do you want to attack? (give the id number)
	 ID: M2 HP: 5/10

** player ATTACK(M2)
      Clang! Wooosh. WHACK!
You successfully attack goblin!

TURN 29
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 99, kill-counts: 9
You are currently in room: R4
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R3
	 Room: Id:R5
There are no monsters in this room.
There are no items in this room.
There are no items in your bag.
      Which Room do you want to move to? (give the id number)
	 ID: R3
	 ID: R5

** player MOVE(R5)
      You try to move to the next room
You successfully moved!

TURN 30
You are in a room. It is dark, and it feels dangerous...
You are alive
Your health: 99, kill-counts: 9
You are currently in room: R5
Your action: Move(m)   | Pick-items(p) | Do-nothing(SPACE) | Quit(q)
             Attack(a) |    Flee(f)    | Use-item(u) 
The connected rooms to this one are:
	 Room: Id:R4
	 Room: Id:E
There are no monsters in this room.
The current items in this room are:
	 Healingpotion: ID:H12
There are no items in your bag.
      Which Room do you want to move to? (give the id number)
	 ID: R4
	 ID: E

** player MOVE(E)
      You try to move to the next room
You successfully moved!
** Aaaw you QUIT! Score:9. Go ahead and brag it out.
